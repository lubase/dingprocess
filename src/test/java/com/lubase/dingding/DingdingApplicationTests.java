package com.lubase.dingding;

import com.lubase.dingding.service.DingTalk2LubaseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = DingdingApplication.class)
class DingdingApplicationTests {
    @Autowired
    DingTalk2LubaseService dingTalk2LubaseService;

    @Test
    void contextLoads() {
    }

    @Test
    void test() {
        String accessToken = dingTalk2LubaseService.getAccessToken();
        System.out.println(accessToken);
    }

    @Test
    void test2() {
        Object userId = dingTalk2LubaseService.getUserInfoByAuthCode("cd8f2d26dc9337949877f6ac70ddfcf6");
        System.out.println(userId);
    }

}
