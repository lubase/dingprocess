package com.lubase.dingding;


import com.alibaba.fastjson.JSON;
import com.lubase.dingding.service.DingProcessService;
import com.lubase.model.DbEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = DingdingApplication.class)
public class DingProcessServiceTest {

    @Autowired
    DingProcessService dingProcessService;

    @Test
    public void testGetAllProcessListByUserCode() {
        String userCode = "admin1";
        List<DbEntity> list = dingProcessService.getAllProcessListByUserCode(userCode);
        System.out.println(JSON.toJSONString(list));
    }

    @Test
    public void testGetAllProcessListByUserId() {
        List<DbEntity> list = dingProcessService.getAllProcessListByUserIdFromQibao();
        System.out.println(JSON.toJSONString(list));
    }
}
