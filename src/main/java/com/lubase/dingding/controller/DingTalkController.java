package com.lubase.dingding.controller;

import com.lubase.core.config.PassToken;
import com.lubase.core.response.ResponseData;
import com.lubase.core.service.flowform.WorkFlowAdapterService;
import com.lubase.dingding.service.DingProcessService;
import com.lubase.dingding.service.DingTalk2LubaseService;
import com.lubase.model.DbEntity;
import com.lubase.orm.model.LoginUser;
import com.lubase.orm.service.AppHolderService;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/dingding")
@RestController
public class DingTalkController {

    @Autowired
    DingTalk2LubaseService dingTalk2LubaseService;

    @Autowired
    AppHolderService appHolderService;
    @Autowired
    DingProcessService dingProcessService;
    @Autowired
    WorkFlowAdapterService workFlowAdapterService;

    @PassToken
    @GetMapping(value = "getSystemToken")
    public ResponseData<LoginUser> login(@NonNull String authCode) {
        if (StringUtils.isEmpty(authCode)) {
            return ResponseData.parameterNotFound("ssoToken");
        }
        //pwd 存放sso_token
        var user = dingTalk2LubaseService.getSSOUser(authCode);
        if (user == null) {
            return ResponseData.error("401", "authCode 错误");
        } else {
            return ResponseData.success(user);
        }
    }

    @GetMapping("getProcessList")
    public ResponseData<List<DbEntity>> getProcessList() {
        List<DbEntity> list = dingProcessService.getAllProcessListByUserId(appHolderService.getUser().getId().toString());

        return ResponseData.success(list);
    }

    @GetMapping("/getApprovalForm")
    public ResponseData<Object> getApprovalForm(String oInsId) {
        if (StringUtils.isEmpty(oInsId)) {
            return ResponseData.parameterNotFound("oInsId");
        }
        return ResponseData.success(workFlowAdapterService.getApprovalFormFromApp(oInsId));
    }

    /**
     * 废弃方法，请使用 getApprovalForm
     *
     * @param oInsId
     * @return
     */
    @GetMapping("/lubase/getApprovalForm")
    public ResponseData<Object> getApprovalForm2(String oInsId) {
        if (StringUtils.isEmpty(oInsId)) {
            return ResponseData.parameterNotFound("oInsId");
        }
        return ResponseData.success(workFlowAdapterService.getApprovalFormFromApp(oInsId));
    }


}
