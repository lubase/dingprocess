package com.lubase.dingding.service;


import com.lubase.model.DbEntity;

import java.util.List;

public interface DingProcessService {
    List<DbEntity> getAllProcessListByUserId(String userId);
    List<DbEntity> getAllProcessListByUserCode(String userCode);

    List<DbEntity> getAllProcessListByUserIdFromQibao();

    String getTokenFromQibao(String deptName,String token);
}
